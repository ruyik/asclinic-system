#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import rospy
import cv2
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import time




class CameraObjectDetector():


    def __init__(self):

        # Subscriber to the image frame from camera_capture.py
        self.sub = rospy.Subscriber("/asc/camera_image", Image, self.load_image_callback)
       

        self.net = cv2.dnn.readNetFromDarknet('/home/asc02/asclinic-system/catkin_ws/src/asclinic_pkg/src/ryuki/yolo/yolov3-tiny.cfg', '/home/asc02/asclinic-system/catkin_ws/src/asclinic_pkg/src/ryuki/yolo/yolov3-tiny.weights')
        self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
        self.counter = 0
        self.ln = self.net.getLayerNames()
        self.ln = [self.ln[i - 1] for i in self.net.getUnconnectedOutLayers()]
        
        self.old_time = rospy.get_time()
        self.current_time = rospy.get_time()
        
        self.classes = open('/home/asc02/asclinic-system/catkin_ws/src/asclinic_pkg/src/ryuki/yolo/coco.names').read().strip().split('\n') # Names of the classes
        np.random.seed(42)
        self.colors = np.random.randint(0, 255, size=(len(self.classes), 3), dtype='uint8') # Colours to use for the bounding boxes
        
        
  
    # Callback when subscriber recieves a frame
    def load_image_callback(self, msg: Image):
        
        
        self.current_time = rospy.get_time()

        # Convert it into into a numpy array to process the image:
        img = CvBridge().imgmsg_to_cv2(msg) # 1920x1080x3

        blob = cv2.dnn.blobFromImage(img, 1/255.0, (416, 416), swapRB=True, crop=False)
        self.net.setInput(blob)
        t0 = time.time()
        outputs = self.net.forward(self.ln)
        

        t = time.time() - t0
        outputs = np.vstack(outputs)
        self.post_process(img, outputs, 0.3)

    def post_process(self, img, outputs, conf):
        H, W = img.shape[:2]

        boxes = []
        confidences = []
        classIDs = []
        
        for output in outputs:
            scores = output[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]
            if confidence > conf: # threshold
                x, y, w, h = output[:4] * np.array([W, H, W, H])
                p0 = int(x - w//2), int(y - h//2)
                p1 = int(x + w//2), int(y + h//2)
                boxes.append([*p0, int(w), int(h)])
                confidences.append(float(confidence))
                classIDs.append(classID)

        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf, conf-0.1)
        
        if len(indices) > 0:
            
            for i in indices.flatten():
                
                if (self.classes[classIDs[i]] == 'potted plant' and (self.current_time - self.old_time) > 5):
                    self.old_time = self.current_time

                    (x, y) = (boxes[i][0], boxes[i][1])
                    (w, h) = (boxes[i][2], boxes[i][3])
                    color = [int(c) for c in self.colors[classIDs[i]]]
                    
                    cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                    text = "{}: {:.4f}".format(self.classes[classIDs[i]], confidences[i])
                    cv2.putText(img, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)

                    cv2.imwrite('/home/asc02/plant_image/plant_' + str(self.counter) + '.jpg', img)
                    self.counter = self.counter + 1
    

if __name__ == '__main__':

    rospy.init_node('camera_object_detector')

    camera_object_detector = CameraObjectDetector()

    rospy.spin()
