#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import math
import numpy as np
from asclinic_pkg.msg import LeftRightFloat32
from asclinic_pkg.msg import LeftRightInt32
from std_msgs.msg import Bool
from asclinic_pkg.msg import PID
from asclinic_pkg.msg import path

kp = 2
kd = 0
ki = 0

radius = 0.072
base = 0.109

class WheelVelocityControl:

    def __init__(self):
        # Intitalise the variables which dictate the desired heading and velocity of the robot
        rospy.logerr('[PID Parameters] Kp {}, Kd {}, Ki {}'.format(kp, kd, ki))
        self.reachedEnd = False
        self.rotatingEnd = False
        self.current_time = 0
        self.time = 0
        self.angular = 0
        self.linear = 0
        self.theta_l = 0
        self.theta_r = 0
        self.theta_l_ = 0
        self.theta_r_ = 0
        self.err_l_ = 0
        self.err_r_ = 0
        self.I_err_l = 0
        self.I_err_r = 0

        # Initialise a publisher which will publoish the PWM to the motors. 
        self.motor_speed_publisher = rospy.Publisher("/set_motor_duty_cycle", LeftRightFloat32 , queue_size=10)
        # Initialise a timer
        self.counter = 0
        rospy.Timer(rospy.Duration(0.05), self.timerCallbackForPublishing)

        # Initialise a subscriber which Subscribes to the heading and velocity outputted by the Path Planning
        rospy.Subscriber("/trajectory_tracking/reference", PID, self.subscriberCallback, queue_size=1)

        # Intitalise a subscriber which Subscribes to the encoder readings. Need to change this reading to current wheel velocity.
        rospy.Subscriber("/encoder_counts", LeftRightInt32, self.subscriberCallbackEncoder, queue_size=1)

        # Initialise a subscirber to see if it has reached the end of the Node
        rospy.Subscriber("/trajectory_tracking/reachedEnd", Bool, self.REsubscriberCallback, queue_size=1)

        rospy.Subscriber("/trajectory_tracking/rotatingEnd", Bool, self.ROEsubscriberCallback, queue_size=1)

        rospy.Subscriber("/path_planning/path", path, self.pathSubscriberCallback)

    # Respond to timer callback
    def timerCallbackForPublishing(self, event):

        msg = LeftRightFloat32()
        
        if (self.reachedEnd == True or self.rotatingEnd == True):
            self.I_err_l = 0
            self.I_err_r = 0
            msg.left = 0
            msg.right = 0
            self.motor_speed_publisher.publish(msg)

            rospy.logerr("pwm left {}, right: {}".format(msg.left, msg.right))
            
        else:

            ref_l = (self.linear / radius) - ((self.angular * base) / radius)
            ref_r = (self.linear / radius) + ((self.angular * base) / radius)

            self.current_time = rospy.get_time()

            dt = self.current_time - self.time
            self.time = self.current_time

            d_theta_l = self.theta_l - self.theta_l_ 
            d_theta_r = self.theta_r - self.theta_r_ 
            
            self.theta_l_ = self.theta_l
            self.theta_r_ = self.theta_r

            angular_l = d_theta_l / dt
            angular_r = d_theta_r / dt

            err_l = ref_l - angular_l
            err_r = ref_r - angular_r

            err_l_dt = (err_l - self.err_l_) / dt
            err_r_dt = (err_r - self.err_r_) / dt

            self.err_l_ = err_l
            self.err_r_ = err_r

            self.I_err_l = self.I_err_l + err_l * dt
            self.I_err_r = self.I_err_r + err_r * dt

            out_angular_l = ref_l + kp * err_l + kd * err_l_dt + ki * self.I_err_l
            out_angular_r = ref_r + kp * err_r + kd * err_r_dt + ki * self.I_err_r

            pwm_l = out_angular_l / 0.15
            pwm_r = out_angular_r / 0.15

            if (pwm_l > 20):
                pwm_l = 20
            elif (pwm_l < -20):
                pwm_l = -20
            if (pwm_r > 20):
                pwm_r = 20
            elif (pwm_r < -20):
                pwm_r = -20

            msg.left = -pwm_l
            
            msg.right = -pwm_r
            
            rospy.logerr("pwm left {}, right: {}".format(msg.left, msg.right))

            # Publish a message
            self.motor_speed_publisher.publish(msg)

        

    # Respond to subscriber receiving a message
    def subscriberCallback(self, msg):
        self.angular = msg.angular
        self.linear = msg.linear

    def REsubscriberCallback(self, msg):
        self.reachedEnd = msg.data

    def ROEsubscriberCallback(self, msg):
        self.rotatingEnd = msg.data

    def subscriberCallbackEncoder(self, msg):
        counts_l = msg.left
        counts_r = msg.right
        self.theta_l = (counts_l / 2240) * 2 * math.pi
        self.theta_r = (counts_r / 2240) * 2 * math.pi

    def pathSubscriberCallback(self, msg):
        if msg.Finished:
            rospy.signal_shutdown("Finished entire trajectory")

if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "wheel_velocity_control"
    rospy.init_node(node_name)
    wheel_velocity_control_object = WheelVelocityControl()
    # Spin as a single-threaded node
    rospy.spin()
