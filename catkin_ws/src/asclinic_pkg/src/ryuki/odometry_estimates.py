#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import math
import numpy as np

from asclinic_pkg.msg import Pose
from asclinic_pkg.msg import ArucoLocalisation
from asclinic_pkg.msg import LeftRightInt32
from asclinic_pkg.msg import path

class odometryNode:

    def __init__(self):
        
        self.radius = 0.072
        self.base = 0.109
        self.theta_l = 0.0
        self.theta_r = 0.0
        self.theta_l_ = 0.0
        self.theta_r_ = 0.0
        self.d_theta_l = None
        self.d_theta_r = None
        self.x = 19
        self.y = -9
        self.phi = np.pi

        self.x_ = 19
        self.y_ = -9
        self.phi_ = np.pi
        
        # Initialise a publisher
        self.pose_publisher = rospy.Publisher(node_name+"/pose", Pose, queue_size=10)

        # Initialise a timer
        rospy.Timer(rospy.Duration(0.03), self.timerCallbackForPublishing)

        # Initialise a subscriber
        rospy.Subscriber("/encoder_counts", LeftRightInt32, self.encoderSubscriberCallback)

        rospy.Subscriber("/aruco_pose", Pose, self.arucoLocalSubscriberCallback)

        rospy.Subscriber("/path_planning/path", path, self.pathSubscriberCallback)

    # Respond to timer callback
    def timerCallbackForPublishing(self, event):
        
        self.d_theta_l = self.theta_l - self.theta_l_
        self.d_theta_r = self.theta_r - self.theta_r_
        
        ds = 0.5 * self.radius * (self.d_theta_l + self.d_theta_r)
        d_phi = (self.radius / (2 * self.base)) * (self.d_theta_l - self.d_theta_r)

        self.x = self.x_ + ds * (math.cos(self.phi_ + 0.5 * d_phi))
        self.y = self.y_ - ds * (math.sin(self.phi_ + 0.5 * d_phi))
        self.phi = self.phi_ + d_phi
     
        self.theta_l_ = self.theta_l
        self.theta_r_ = self.theta_r
        self.x_ = self.x
        self.y_ = self.y
        
        publish = Pose()
        publish.x = self.x
        publish.y = self.y

        if (self.phi > np.pi):
            self.phi = self.phi - 2*np.pi
        elif (self.phi < -np.pi):
            self.phi = self.phi + 2*np.pi
            
        self.phi_ = self.phi
        publish.phi = self.phi

        self.pose_publisher.publish(publish)

    # Respond to subscriber receiving a message
    def encoderSubscriberCallback(self, msg):
        counts_l = -msg.left
        counts_r = -msg.right
        self.theta_l = (counts_l / 2240) * 2 * math.pi
        self.theta_r = (counts_r / 2240) * 2 * math.pi
    
    def arucoLocalSubscriberCallback(self, msg):
        self.x_ = msg.x
        self.y_ = msg.y
        self.phi_ = msg.phi

        self.x = msg.x
        self.y = msg.y
        self.phi = msg.phi
        rospy.logerr("Marker Pose update = x: {}, y: {}, phi: {}".format(self.x, self.y, np.rad2deg(self.phi)))
    
    def pathSubscriberCallback(self, msg):
        if msg.Finished:
            rospy.signal_shutdown("Finished entire trajectory")


if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "odometry"
    rospy.init_node(node_name)
    template_py_node = odometryNode()
    # Spin as a single-threaded node
    rospy.spin()
