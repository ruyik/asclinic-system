#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Import the ROS-Python package
import rospy
import numpy as np
import math
import cv2
import cv2.aruco as aruco
from cv_bridge import CvBridge

# Import the standard message types
from std_msgs.msg import UInt32
from sensor_msgs.msg import Image
from std_msgs.msg import Bool
from geometry_msgs.msg import Pose2D
from asclinic_pkg.msg import Pose
from asclinic_pkg.msg import path

# Define the Camera Parameters 
USB_CAMERA_DEVICE_NUMBER = 0
DESIRED_CAMERA_FRAME_HEIGHT = 1080
DESIRED_CAMERA_FRAME_WIDTH = 1920
DESIRED_CAMERA_FPS = 5
MARKER_SIZE = 0.250
# this image path 
SAVE_IMAGE_PATH = "/home/asc02/asclinic-system/image_outputs/" 
# > A flag for whether to display the images captured
SHOULD_SHOW_IMAGES = False
# > A flag for whether to publish the images captured
SHOULD_PUBLISH_CAMERA_IMAGES = True

DO_AVG_AM_POSE_CALC = False
# ADJUST These are the pose of the aruco marker with respect to the origin frame. ADjsut based on position of Aruco marker locations we decide to use
ORIGIN_TO_ARUCO = {2: Pose2D(15.5, -9.0, 0), 4: Pose2D(16.0, -6.0, -np.pi/2), 13: Pose2D(13.0, -6.5, 0), 8: Pose2D(14.0, -4.0, -np.pi/2), 23: Pose2D(13.5, -1.0, -np.pi/2), 7: Pose2D(16.5, -2.5, np.pi), 28: Pose2D(16.0, -5.0, np.pi/2), 5: Pose2D(19, -4.5, np.pi) } 

ARUCO_IDS_USED = [2, 4, 13, 8, 23, 7, 28, 5]

def normalize_angle(angle):
    if (angle>np.pi):
        normal = angle - 2*np.pi
    elif (angle<-(np.pi)):
        normal = angle + 2*np.pi
    else:
        normal = angle
    return normal

class ArucoDetector:

    def __init__(self):
        # Initialise a publisher for the images
        self.image_publisher = rospy.Publisher("/asc"+"/camera_image", Image, queue_size=10)
        
        # Initialise a timer for capturing the camera frames
        self.camera_fps = DESIRED_CAMERA_FPS
        
        # Initialise a subscriber for flagging when to save an image
        rospy.Subscriber("/asc"+"/request_save_image", UInt32, self.requestSaveImageSubscriberCallback)

        # Initialise a subscriber to see when it reaches the end of a node to node path 
        rospy.Subscriber("/trajectory_tracking/reachedEnd", Bool, self.reachedEndSubscriberCallback)
        rospy.Subscriber("/trajectory_tracking/rotatingEnd", Bool, self.rotatingEndSubscriberCallback)

        self.reachedEnd = False
        self.rotatingEnd = False

        # Initialise a publisher for AM pose estimate and covariance estimations3
        self.pose_publisher = rospy.Publisher('/aruco_pose', Pose, queue_size=5)

        rospy.Subscriber("/path_planning/path", path, self.pathSubscriberCallback)
        
        # Initialise variables
        self.save_image_counter = 0
        self.should_save_image = False # IMPORTANT, determines whether to save images from the start 
        self.camera_frame_width  = DESIRED_CAMERA_FRAME_WIDTH
        self.camera_frame_height = DESIRED_CAMERA_FRAME_HEIGHT
        self.camera_fps = DESIRED_CAMERA_FPS
        self.camera_setup = USB_CAMERA_DEVICE_NUMBER
        self.cam=cv2.VideoCapture(self.camera_setup)
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, self.camera_frame_height)
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH,  self.camera_frame_width)
        self.cam.set(cv2.CAP_PROP_FPS, self.camera_fps)
        self.cam.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        self.cam.set(cv2.CAP_PROP_FOCUS, 0)
        self.cam.set(cv2.CAP_PROP_BUFFERSIZE, 1)

        # Set the FPS of the camera to be an approriate value if the one desired is outside the bounds of the camera
        camera_actual_fps = self.cam.get(cv2.CAP_PROP_FPS)
        if not(camera_actual_fps==self.camera_fps):
            self.camera_fps = camera_actual_fps

        # Set up Aruco Detecter Paramaters
        self.cv_bridge = CvBridge()
        self.aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_50)
        self.aruco_parameters = aruco.DetectorParameters()
        self.aruco_parameters.cornerRefinementMethod = aruco.CORNER_REFINE_SUBPIX
        self.aruco_detector = aruco.ArucoDetector(self.aruco_dict, self.aruco_parameters)
        marker_size_half = 0.5 * MARKER_SIZE
        self.single_marker_object_points = np.array([  \
                [-marker_size_half, marker_size_half, 0.0], \
                [ marker_size_half, marker_size_half, 0.0], \
                [ marker_size_half,-marker_size_half, 0.0], \
                [-marker_size_half,-marker_size_half, 0.0]  \
                ], dtype=np.float32 )
        
        # Specify the intrinsic parameters of the camera - Changed these
        self.intrinic_camera_matrix = np.load("/home/asc02/camera_parameters/mtx.npy")
        self.intrinic_camera_distortion = np.load("/home/asc02/camera_parameters/dist.npy")

        # Run a test image capture to ensure all camera variables are set correctly
        return_flag , current_frame = self.cam.read()
        dimensions = current_frame.shape

        if (not(dimensions[0]==self.camera_frame_height) or not(dimensions[1]==self.camera_frame_width)):
            self.camera_frame_height = dimensions[0]
            self.camera_frame_width  = dimensions[1]

        rospy.Timer(rospy.Duration(0.2), self.timerCallbackForCameraRead)

    def timerCallbackForCameraRead(self, event):
        
        return_flag , current_frame = self.cam.read()
        if (return_flag == True):
            
            current_frame_gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)
            aruco_corners_of_all_markers, aruco_ids, aruco_rejected_img_points = self.aruco_detector.detectMarkers(current_frame_gray)

            if aruco_ids is not None:

                current_frame_with_marker_outlines = aruco.drawDetectedMarkers(current_frame.copy(), aruco_corners_of_all_markers, aruco_ids, borderColor=(0, 220, 0))

                rvec_list = []
                tvec_list = []
                Rmat_list = []

                for i_marker_id in range(len(aruco_ids)):

                    this_id = aruco_ids[i_marker_id]
                    
                    if (this_id in ARUCO_IDS_USED):
                        
                        corners_of_this_marker = np.asarray(aruco_corners_of_all_markers[i_marker_id][0], dtype=np.float32)
                        solvepnp_method = cv2.SOLVEPNP_IPPE_SQUARE
                        success_flag, rvec, tvec = cv2.solvePnP(self.single_marker_object_points, corners_of_this_marker, self.intrinic_camera_matrix, self.intrinic_camera_distortion, flags=solvepnp_method)
                        current_frame_with_marker_outlines = cv2.drawFrameAxes(current_frame_with_marker_outlines, self.intrinic_camera_matrix, self.intrinic_camera_distortion, rvec, tvec, 0.5*MARKER_SIZE)
                        Rmat = cv2.Rodrigues(rvec)[0]

                        rvec_list.append(rvec)
                        tvec_list.append(tvec)
                        Rmat_list.append(Rmat)

                self.process_tvec_rvec(tvec_list, rvec_list,Rmat_list, aruco_ids[:,0])
                

            else:
                current_frame_with_marker_outlines = current_frame_gray

            if (SHOULD_PUBLISH_CAMERA_IMAGES):
                self.image_publisher.publish(self.cv_bridge.cv2_to_imgmsg(current_frame))

            if (self.should_save_image):
                self.save_image_counter += 1
                id_rvec_tvec_string = "id = " + str(this_id) + ", tvec = [ " + str(tvec[0]) + " , " + str(tvec[1]) + " , " + str(tvec[2]) + " ]" + ", rvec = [ " + str(rvec[0]) + " , " + str(rvec[1]) + " , " + str(rvec[2]) + " ]" 
                temp_filename = SAVE_IMAGE_PATH + str(self.save_image_counter) + id_rvec_tvec_string + "aruco_image" + ".jpg"
                cv2.imwrite(temp_filename,current_frame_with_marker_outlines)
                self.should_save_image = False
            if (SHOULD_SHOW_IMAGES):
                cv2.imshow("[ARUCO DETECTOR]", current_frame_with_marker_outlines)
        else:
            pass

    # Respond to subscriber receiving a message
    def requestSaveImageSubscriberCallback(self, msg):
        self.should_save_image = True

    def reachedEndSubscriberCallback(self, msg):
        self.reachedEnd = msg.data

    def rotatingEndSubscriberCallback(self, msg):
        self.rotatingEnd = msg.data

    def process_tvec_rvec(self, tvec_list, rvec_list, Rmat_list, aruco_ids):
        if (((self.reachedEnd == True) or (self.rotatingEnd == True)) and (len(tvec_list)>0)):

            small_dist = 99999
            for i_marker_id in range(len(tvec_list)):
                tvec = tvec_list[i_marker_id]
                norm_dist = np.linalg.norm(tvec, axis=0)
                if (norm_dist< small_dist):
                    closest_ID_index = i_marker_id
                    small_dist = norm_dist

            tvec_closest = tvec_list[closest_ID_index]
            rvec_closest = rvec_list[closest_ID_index]
            Rmat_closest = Rmat_list[closest_ID_index]
            closest_id = aruco_ids[closest_ID_index]

            if (closest_id in ARUCO_IDS_USED):
                zOM_0 = 0
                xOM_0 = ORIGIN_TO_ARUCO[closest_id].x
                yOM_0 = ORIGIN_TO_ARUCO[closest_id].y
                thetaOM_O = ORIGIN_TO_ARUCO[closest_id].theta

                Rmi = np.matrix([[math.cos(thetaOM_O), 0, math.sin(thetaOM_O)],
                                [0, 1, 0],
                                [-math.sin(thetaOM_O), 0, math.cos(thetaOM_O)]])

                Rcm = Rmat_closest
                ti = np.array([[yOM_0],[zOM_0],[xOM_0]])
                pos = ti - Rmi @ Rcm @ tvec
                A_MtoC = rvec_closest[1]
                phi = thetaOM_O + A_MtoC

                msg = Pose()
                msg.x = pos[2]
                msg.y = pos[0]
                
                msg.phi = normalize_angle(phi)
                
                if thetaOM_O == np.pi/2:
                    msg.phi = msg.phi + np.pi
                elif thetaOM_O == -np.pi/2:
                    msg.phi = msg.phi - np.pi
                
                if (small_dist <= 3.5):
                    self.pose_publisher.publish(msg)
                    rospy.logerr('The ID marker seen is ' + str(closest_id))
                    rospy.logerr('The distance to seen marker is ' + str(small_dist))
                
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ # 


    def pathSubscriberCallback(self, msg):
        if msg.Finished:
            rospy.signal_shutdown("Finished entire trajectory")

if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "aruco_detector"
    rospy.init_node(node_name)
    aruco_detector_object = ArucoDetector()
    # Spin as a single-threaded node
    
    rospy.spin()

    # Release the camera
    aruco_detector_object.cam.release()
    # Close any OpenCV windows
    cv2.destroyAllWindows()
