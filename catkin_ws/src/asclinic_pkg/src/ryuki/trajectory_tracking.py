#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import math

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
from asclinic_pkg.msg import Pose
from asclinic_pkg.msg import PID
from asclinic_pkg.msg import path
from std_msgs.msg import Bool

def normalize_angle(angle):
    if (angle>np.pi):
        normal = angle - 2*np.pi
    elif (angle<-(np.pi)):
        normal = angle + 2*np.pi
    else:
        normal = angle
    return normal

class trajectory_tracking_pure_persuit:

    def __init__(self):
        # Initialise a publisher to publish reference velocities
        self.PID_publisher = rospy.Publisher("trajectory_tracking"+"/reference", PID, queue_size=10)

        # Initialise a publisher to publish a flag signaling reached path points
        self.reachedEnd_publisher = rospy.Publisher("trajectory_tracking"+"/reachedEnd", Bool, queue_size=10)

        # Initialise a publisher to publish a flag signaling completed heading correction
        self.rotatingEnd_publisher = rospy.Publisher("trajectory_tracking"+"/rotatingEnd", Bool, queue_size=10)

        # Initialise a timer
        rospy.Timer(rospy.Duration(0.25), self.timerCallbackForPublishing)

        # Initialise a subscriber to get new path point for the robot to reach
        rospy.Subscriber("/path_planning/path", path, self.pathSubscriberCallback)
        
        # Initialise a subscriber to get current pose of the robot
        rospy.Subscriber("/odometry/pose", Pose, self.poseSubscriberCallBack)

        self.x_pos = 0.0
        self.y_pos = 0.0
        self.theta = 0.0
        self.new_path = False
        self.path_x = None
        self.path_y = None
        self.Finished = False
        self.trajectory_x = []
        self.trajectory_y = []
        
    # Respond to timer callback
    def timerCallbackForPublishing(self, event):
        if self.path_x is not None and (not self.Finished):
            # Travel straight when not reached point
            if self.new_path == False:
                if (((abs(self.x_pos - self.path_x[-1]) < 0.04) and (abs(self.path_x[-1]- self.path_x[0]) == 0.5)) or ((abs(self.y_pos - self.path_y[-1]) < 0.04) and (abs(self.path_y[-1]- self.path_y[0]) >= 0.5))):
                    self.reachedEnd_publisher.publish(True)
                    self.trajectory_x.append(self.x_pos)
                    self.trajectory_y.append(self.y_pos)
                    rospy.logerr('Reached point. Stop.')
                msg = PID()
                msg.angular = 0
                msg.linear = 0.5
                self.PID_publisher.publish(msg)
            # Correct the heading before going to the next point
            elif self.new_path == True:
                self.reachedEnd_publisher.publish(False)
                self.rotatingEnd_publisher.publish(False)

                theta_wanted = -math.atan2((self.path_y[-1]-self.path_y[0]), (self.path_x[-1] - self.path_x[0]))
                theta_current = self.theta
                theta_difference =  theta_wanted - theta_current
                theta_difference = normalize_angle(theta_difference)

                msg = PID()

                while(abs(theta_difference)>0.08):

                    if(theta_difference<0):
                        msg.angular = 1
                        msg.linear = 0 
                    else:
                        msg.angular = -1
                        msg.linear = 0
                    
                    self.PID_publisher.publish(msg)

                    theta_current = normalize_angle(self.theta)
                    theta_difference =  theta_wanted - theta_current
                    theta_difference = normalize_angle(theta_difference)

                    rateRU = rospy.Rate(30)
                    rateRU.sleep()
                
                rospy.logerr('ROTATION END!')
                msg.angular = 0
                msg.linear = 0
                self.PID_publisher.publish(msg)
                self.rotatingEnd_publisher.publish(True)
                
                rate = rospy.Rate(1)
                rate.sleep()

                self.rotatingEnd_publisher.publish(False)
                self.new_path = False
        elif self.Finished:
            plt.plot(self.trajectory_y, self.trajectory_x)
            plt.savefig('/home/asc02/image_outputs/trajectory.png')
            
            rospy.signal_shutdown("Finished entire trajectory")

    # Respond to subscriber receiving a message
    def pathSubscriberCallback(self, msg):
        self.new_path = True
        self.path_x = msg.x
        self.path_y = msg.y
        self.Finished = msg.Finished
        if not self.Finished:
            rospy.logerr("path x: {}, y: {}".format(self.path_x, self.path_y))

    def poseSubscriberCallBack(self, msg):
        self.x_pos = msg.x
        self.y_pos = msg.y
        self.theta = msg.phi
        rospy.logerr("pose x: {}, y: {}, phi: {}".format(self.x_pos, self.y_pos, np.rad2deg(self.theta)))

if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "trajectory_tracking_pure_persuit"
    rospy.init_node(node_name)
    trajectory_tracking_node = trajectory_tracking_pure_persuit()
    # Spin as a single-threaded node
    rospy.spin()
