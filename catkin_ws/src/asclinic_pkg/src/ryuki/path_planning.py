#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
#
# This file is part of ASClinic-System.
#    
# See the root of the repository for license details.
#
# ----------------------------------------------------------------------------
#     _    ____   ____ _ _       _          ____            _                 
#    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
#   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
#  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
# /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
#                                                 |___/                       
#
# DESCRIPTION:
# Template Python node with a publisher and subscriber
#
# ----------------------------------------------------------------------------


# Import the ROS-Python package
import rospy
import numpy as np
import math
from asclinic_pkg.msg import path
from asclinic_pkg.msg import Pose
from std_msgs.msg import Bool

class path_planning_node:

    def __init__(self):
        self.publish_path = rospy.Publisher(node_name+"/path", path, queue_size=10)
        self.rate = rospy.Rate(1) # 1 Hz
        rospy.Subscriber("/trajectory_tracking/reachedEnd", Bool, self.reachedEndSubscriberCallback)
    
        rospy.Timer(rospy.Duration(2), self.path_planning)
        rospy.logerr("Initialised")

        self.reachedEnd = False
        self.Finished = False

    def path_planning(self, event):
        #rospy.logerr("begin")
        path_points = path()
        
        path_array = [(19, -9), (18.5, -9.0), (18.0, -9.0), (17.5, -9.0), (17.0, -9.0), (16.5, -9.0), (16.0, -9.0), 
                    (16.0, -8.5), (16.0, -8.0), (16.0, -7.5), (16.0, -7.0), (16.0, -6.5), 
                    (15.5, -6.5), (15.0, -6.5), (14.5, -6.5), (14.0, -6.5), (13.5, -6.5),  
                    (13.5, -6.0), (13.5, -5.5), (13.5, -5.0), (13.5, -4.5), (13.5, -4.0), (13.5, -3.5), (13.5, -3.0), (13.5, -2.5),
                    (14.0, -2.5), (14.5, -2.5), (15.0, -2.5), (15.5, -2.5), (16.0, -2.5), 
                    (16.0, -3.0), (16.0, -3.5), (16.0, -4.0), (16.0, -4.5),
                    (16.5, -4.5), (17.0, -4.5), (17.5, -4.5), (18.0, -4.5), (18.5, -4.5)]
                    
        for i in range (0, len(path_array)-1):
            self.reachedEnd = False
            path_x = [path_array[i][0], path_array[i+1][0]]
            path_y = [path_array[i][1], path_array[i+1][1]]

            path_points.x = path_x
            path_points.y = path_y
            path_points.Finished = False

            self.publish_path.publish(path_points)

            while(self.reachedEnd == False):
                self.rate.sleep()
        
        path_points.x = [0, 0]
        path_points.y = [0, 0]
        path_points.Finished = True
        self.publish_path.publish(path_points)

        rospy.signal_shutdown("Finished entire trajectory")

    def reachedEndSubscriberCallback(self, msg):
        self.reachedEnd = msg.data

if __name__ == '__main__':
    
    global node_name
    node_name = "path_planning"
    rospy.init_node(node_name)
    
    path_planning_node = path_planning_node()
    # # SPIN AS A SINGLE-THREADED NODE
    rospy.spin() 
    
    
    
