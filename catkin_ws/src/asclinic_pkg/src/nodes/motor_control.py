#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
#
# This file is part of ASClinic-System.
#    
# See the root of the repository for license details.
#
# ----------------------------------------------------------------------------
#     _    ____   ____ _ _       _          ____            _                 
#    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
#   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
#  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
# /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
#                                                 |___/                       
#
# DESCRIPTION:
# Template Python node with a publisher and subscriber
#
# ----------------------------------------------------------------------------





# Import the ROS-Python package
import rospy
import numpy as np
# Import the standard message types
from std_msgs.msg import UInt32
from collections import deque
from geometry_msgs.msg import Twist
# Import the asclinic message types
from asclinic_pkg.msg import LeftRightFloat32,LeftRightInt32
from nav_msgs.msg import Odometry
from math import pi
import math
import time
from asclinic_pkg.msg import Pose
SAVE_PATH = "/home/asc02/saved_step_response/"

# NOTE:
# A number of the function description given in this file are
# taken directly from the following ROS tutorials:
# http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29
# http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29
# http://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers
# http://wiki.ros.org/rospy/Overview/Time





class MotorControl:

    def __init__(self):
        self.x=-0.5
        self.y=1
        self.phi= pi
        self.left_w = 0
        self.right_w = 0

        self.covp_t_t1 = np.eye(3)
        self.p_tt1 = np.array([[self.x],[self.y],[self.phi]])

        # INITIALISE THE PUBLISHER
        # Initialise the member variable "self.template_publisher" to
        # be a "Publisher" type variable. The publisher needs to be
        # a member variable because it is used in other functions when
        # publishing a message. The syntax for the call to initialise
        # the publisher is:
        #   pub = rospy.Publisher("topic_name", std_msgs.msg.String, queue_size=10, latch=False)
        # where:
        #   "topic_name"
        #     String that defines the name under which the topic is
        #     advertised for publication.
        #   std_msgs.msg.String
        #     Specifies the type that defines the structure of the
        #     message to be communicated on this topic. The commonly
        #     used standard message types, i.e., "std_msgs", are:
        #       std_msgs.msg.Bool
        #       std_msgs.msg.UInt32
        #       std_msgs.msg.Int32
        #       std_msgs.msg.Float32
        #       std_msgs.msg.String
        #       std_msgs.msg.Time
        #       std_msgs.msg.Duration
        #     Custom message type have the structure defined by
        #     files in the "msg" folder of the package, i.e., look
        #     for the "TemplateMessage.msg" file for the example
        #     used below.
        #   "topic_name"
        #     String that defines the name under which the topic is
        #     advertised for publication.
        #   queue_size=10
        #     Integer that defines the number of messages buffered.
        #   latch=False
        #     Enables "latching" on a connection. When a connection
        #     is latched, the last message published is saved and
        #     automatically sent to any future subscribers that connect.
        #     This is useful for slow-changing to static data like a map.
        #     Default is false.
        #
        # 
        # The rospy.Publisher() function is how you tell ROS that you want
        # to publish on a given topic name. This invokes a call to the ROS
        # master node, which keeps a registry of who is publishing and who
        # is subscribing. After this rospy.Publisher() call is made, the master
        # node will notify anyone who is trying to subscribe to this topic name,
        # and they will in turn negotiate a peer-to-peer connection with this
        # node.  rospy.Publisher() returns a Publisher object which allows you to
        # publish messages on that topic through a call to publish().
        #
        # The queue_size parameters means that if messages are published more
        # quickly than we can send them, the number here specifies how many
        # messages to buffer up before throwing some away.
        #
        #self.template_publisher = rospy.Publisher("/asc"+"/encoder_counts", LeftRightInt32, queue_size=10)
        
        # INITIALISE THE TIMER FOR RECURRENT PUBLISHING
        # Initialise a local variable to be "rospy.Timer" type variable.
        # The syntax for the call to initialise the timer is:
        #   rospy.Timer(period, callback, oneshot=False)
        # where:
        #   period
        #     This is the period between calls to the timer callback. For
        #     example, if this is rospy.Duration((0.1), the callback will be
        #     scheduled for every 1/10th of a second.
        #   callback
        #     This is the callback to be called. The function is passed a
        #     TimerEvent instance.
        #   oneshot=False
        #     Specifies whether or not the timer is a one-shot timer.
        #     If so, it will only fire once. Otherwise it will be re-scheduled
        #     continuously until it is stopped.
        #
        self.counter = 0
        rospy.Timer(rospy.Duration(0.5), self.timerCallbackForPublishing, oneshot=False)

        # INITIALISE THE SUBSCRIBER
        # Initialise a local variable to be a "rospy.Subscriber" type variable
        # that subscribes to the topic named "template_topic". The subscriber
        # can be a local variable because as long as this instance of the 
        # "TemplatePyNode" class exists, i.e., as long as this node is active
        # as per the "__main__" function, then the subsriber will trigger the
        # callback function when messages are received. The syntax for the call
        # to initialise the subscriber is:
        #   rospy.Subscriber("topic_name", std_msgs.msg.String, callback queue_size=10)
        # where:
        #   topic
        #     String that defines the name of which topic to subscribe to.
        #   callback
        #     This is the callback to be called, most commonly is a class method.
        #   queue_size=10
        #     Integer that defines the number of messages buffered.
        #
        #
        # The rospy.Subscriber() call is how you tell ROS that you want to receive 
        # messages on a given topic.  This invokes a call to the ROS
        # master node, which keeps a registry of who is publishing and who
        # is subscribing.  Messages are passed to a callback function, here
        # called chatterCallback.  rospy.Subscriber() returns a Subscriber object that you
        # must hold on to until you want to unsubscribe.
        #
        # The queue_size parameters means that if messages are arriving faster than they
        # are being processed, this is the number of messages that will be buffered up
        # before beginning to throw away the oldest ones.
        #
        
        self.set_PWM_publisher = rospy.Publisher("/asc"+"/set_motor_duty_cycle", LeftRightFloat32, queue_size=10)
        self.odom_publisher = rospy.Publisher("/asc"+"/odom", Odometry, queue_size=1)
        rospy.Subscriber("/asc"+"/cv_pose", Pose, self.updatecallback)
        rospy.Subscriber("/asc"+"cmd_vel", Twist, self.RefrenceCallback)
        rospy.Subscriber("/asc"+"/encoder_counts", LeftRightInt32, self.templateSubscriberCallback)
        rate = rospy.Rate(50)
        self.smg_PWM = LeftRightFloat32()
        self.odom = Odometry()

        self.WL = deque([0,0,0,0])
        self.WR = deque([0,0,0,0])
        self.t0 = time.time()
        self.PWM_L = 0
        self.PWM_R = 0
        self.error_i_L = 0
        self.error_i_R = 0
        self.error_L_last = 0
        self.error_R_last = 0
        self.error_L_d = 0
        self.error_R_d = 0
        self.save_angular_velocity_L = np.array([])
        self.save_angular_velocity_R = np.array([])
        self.ref_left_w = 0
        self.ref_right_w = 0
        self.r=7.1e-2*1.8*2.9/3.17/2.933*3.4
        self.b=21.6e-2 *0.7/1.6
        self.rotate_factor = 1
        self.Ldir = 1
        self.Rdir = 1

    # Respond to timer callback
    def timerCallbackForPublishing(self, event):
        # Increment the counter
        self.counter += 1
        # PUBLISH A MESSAGE
        # Initialise a "TemplateMessage" struct
        # > Note that all value are set to zero by default
    def RefrenceCallback(self,msg):
        
        self.ref_left_w = (msg.linear.x+msg.angular.z*self.b)/self.r
        self.ref_right_w = (msg.linear.x-msg.angular.z*self.b)/self.r
        if self.ref_left_w < 0:
            self.ref_left_w = 0
        if self.ref_right_w < 0:
            self.ref_right_w = 0
        
        #print('encoder obs L R',self.left_w,self.right_w)
        #print('ref  L R',self.ref_left_w,self.ref_right_w)
        #print('ref angular z ',msg.angular.z)
        if msg.angular.y == 1:
            self.rotate_factor = abs(msg.angular.z)
            if msg.angular.z > 0:
                self.Ldir = -1
                self.Rdir = 1
            else:
                self.Ldir = 1
                self.Rdir = -1
        else:
                self.Ldir = 1
                self.Rdir = 1
        #print("\nBody velocity: ",msg.linear.x, msg.angular.z)
        #print("left_w_ref",self.ref_left_w)
        #print("right_w_ref",self.ref_right_w)
    
    def updatecallback(self,msg):
        
        cov_z = np.array([[0.0001,0,0],[0,0.00000001,0],[0,0,0.1]])
        Kt = self.covp_t_t1  @ np.linalg.inv(self.covp_t_t1 + cov_z)
        self.p_tt = self.p_tt1 + Kt*(np.array([[msg.x],[msg.z],[msg.phi]]) - self.p_tt1)
        self.cov_tt = self.covp_t_t1 - Kt@(self.covp_t_t1 + cov_z)@ Kt.T
        
        # 
        
        self.covp_t_t1 = self.cov_tt

        #print('pp t t-1', self.p_tt1)
        #print('Kt gain', Kt)
        #print(msg.pose.pose.position)

        self.x = self.p_tt[0,0]
        self.y = self.p_tt[1,0]
        self.phi = self.p_tt[2,0]

        # dir update
        self.x = msg.x
        self.y = msg.z
        self.phi = msg.phi
       # print('phi',self.phi , 'x',self.x ,'y',self.y)

    # Respond to subscriber receiving a message
    def templateSubscriberCallback(self, msg):
        # Display that a message was received
        dt = 0.02
        left_w =  msg.left *2*pi/4400/dt
        right_w = msg.right *2*pi/4400/dt

        if self.Ldir != self.Rdir:
            left_w = -self.Ldir * msg.left *2*pi/4400/dt
            right_w = -self.Rdir * msg.right *2*pi/4400/dt

        # if rotate : 
        

        self.WL.appendleft(left_w)
        self.WL.pop()
        self.WR.appendleft(right_w)
        self.WR.pop()

        self.left_w = np.mean(self.WL)
        self.right_w = np.mean(self.WR)
        #####
        #self.left_w = self.Ldir * msg.left *2*pi/4400/dt
        #self.right_w = self.Rdir * msg.right *2*pi/4400/dt
        

        d_st=self.r*self.left_w*dt/2 + self.r*self.right_w*dt/2
        d_phit = self.r*self.left_w*dt/2/self.b - self.r*self.right_w*dt/2/self.b
        self.x +=  d_st*math.cos(self.phi+0.5*d_phit)
        self.y +=  d_st*math.sin(self.phi+0.5*d_phit)
        self.phi += d_phit
        # predict kalman state
        self.p_tt1 = np.array([[self.x],[self.y],[self.phi]])
        
        #print('low :  x:',self.x,'\n y:',self.y,'\n phi:',self.phi)
        #print("\nwheel speed ref: ", self.ref_left_w, self.ref_right_w)
        #print("\nwheel speed: ",self.left_w, self.right_w)
       # print('speed is:',msg)
        #if time.time() - self.t0 < 10:
            #print('ticks ',msg )
            #print('left w is:',self.left_w ,'rad/s')
            #print('right w is:',self.right_w ,'rad/s')


        # sat to angle [-pi , pi]
        if self.phi > pi:
            self.phi = self.phi -2*pi
        if self.phi < -pi:
            self.phi = self.phi + 2*pi
        
        self.odom.twist.twist.linear.x = self.r * left_w/2/dt + self.r*right_w/2/dt
        self.odom.twist.twist.angular.z = self.r*right_w/2/self.b/dt - self.r*left_w/2/self.b/dt
        self.odom.pose.pose.position.x = self.x
        self.odom.pose.pose.position.y = self.y
        self.odom.pose.pose.orientation.x = self.phi
        #print(self.odom)
        self.odom_publisher.publish(self.odom)

        Kp_R = 20
        Ki_R = 1
        Kd_R = 10

        Kp_L = 25
        Ki_L = 6
        Kd_L = 10

        error_L = self.ref_left_w - self.left_w
        error_R = self.ref_right_w - self.right_w

        self.error_L_d = (error_L - self.error_L_last)/dt
        self.error_R_d = (error_R - self.error_R_last)/dt


        self.error_i_L +=  error_L
        self.error_i_R +=  error_R

        self.PWM_L = error_L * Kp_L + self.error_i_L * Ki_L + self.error_L_d * Kd_L
        self.PWM_R = error_R * Kp_R + self.error_i_R * Ki_R + self.error_R_d + Kd_R
        if self.error_i_L < -20:
            self.error_i_L = -20
        if self.error_i_L > 20:
            self.error_i_L = 20


        if self.error_i_R < -40:
            self.error_i_R = -40
        if self.error_i_R > 40:
            self.error_i_R = 40

        self.error_L_last = error_L
        self.error_R_last = error_R

        
        if self.PWM_L > 70:
            self.PWM_L = 70
        if self.PWM_R > 70:
            self.PWM_R = 70

        if self.PWM_L < -70:
            self.PWM_L = -70
        if self.PWM_R < -70:
            self.PWM_R = -70

        

        #print('PWM L R',self.smg_PWM.left,self.smg_PWM.right)
        self.smg_PWM.left = -self.PWM_L -6
        self.smg_PWM.right = -self.PWM_R -6
        
        # rotate : 
        if self.Ldir != self.Rdir:
            #print('dir L R ',self.Ldir,self.Rdir)
           # print('self.rotate_factor',self.rotate_factor)
            self.smg_PWM.left  = 15 * self.Ldir * (self.rotate_factor + 0.4)
            self.smg_PWM.right = 15 * self.Rdir * (self.rotate_factor + 0.4)

        if self.ref_left_w==0 and self.ref_right_w == 0:
            self.smg_PWM.left = 0 
            self.smg_PWM.right = 0
        if time.time() - self.t0 > 100:
            self.smg_PWM.left = 0 
            self.smg_PWM.right = 0

        #print('left',self.smg_PWM.left , 'right',self.smg_PWM.right)
        #print('sent\n',self.smg_PWM)
        self.set_PWM_publisher.publish(self.smg_PWM)

        self.save_angular_velocity_L = np.append(self.save_angular_velocity_L,self.left_w)
        self.save_angular_velocity_R = np.append(self.save_angular_velocity_R,self.right_w)

        #if len(self.save_angular_velocity_L) ==  400:
           # np.savetxt(SAVE_PATH + 'velocity_L.out',self.save_angular_velocity_L,delimiter=',')
         #   np.savetxt(SAVE_PATH + 'velocity_R.out',self.save_angular_velocity_R,delimiter=',')



        

if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "motor_control"
    rospy.init_node(node_name)
    aruco_detector_object = MotorControl()
    # Spin as a single-threaded node
    rospy.spin()

