#!/usr/bin/env python
# -*- coding: utf-8 -*-
#motor controller takes input from trajectory controller (wheel angular velocity), feedback from encoders, 
#outputs to motors as pwm voltages (percentage)


import rospy
import math
from asclinic_pkg.msg import LeftRightInt32
from asclinic_pkg.msg import LeftRightFloat32
from asclinic_pkg.msg import TemplateMessage

TIMESTEP = 1/30 #must match encoder publishing frequency
PWM_MAX = 50
KP = 5
KI = 0.0
KD = 0.0

#to be replaced with a standard library for PID controllers.
class MotorController:
    def __init__(self, kp, ki, kd, min_output, max_output):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.min_output = min_output
        self.max_output = max_output
        self.error = 0.0
        self.previous_error = 0.0
        self.integral = 0.0
        self.derivative = 0.0
        self.cpr = 2240
        self.ff_factor = 7 #feedforwardfactor, linear relationship between speed and voltage, experimentally derived 
        self.ff_offset = 7 #feedforwardoffset, units are voltage percentage/speed 

    def compute(self, setpoint, feedback): 
        self.error = setpoint - feedback
        self.integral += self.error
        self.derivative = self.error - self.previous_error
        if setpoint > 0.01:
            output = (self.kp * self.error) + (self.ki * self.integral) + (self.kd * self.derivative) + (self.ff_factor * setpoint) + self.ff_offset
            output = min(max(output, self.min_output), self.max_output)
        elif setpoint < 0.01:
            output = (self.kp * self.error) + (self.ki * self.integral) + (self.kd * self.derivative) + (self.ff_factor * setpoint) - self.ff_offset
            output = min(max(output, self.min_output), self.max_output)
        else:
            output = 0.0
        self.previous_error = self.error
        return output

class MotorControllerNode:
    
    def __init__(self):
        self.controller = MotorController(kp=KP, ki=KI, kd=KD, min_output=-PWM_MAX, max_output=PWM_MAX)
        self.d_theta_left = 0
        self.d_theta_right = 0
        self.setpoint = LeftRightFloat32()
        self.setpoint.left = 0
        self.setpoint.right = 0
        self.left_counts_old = 0
        self.right_counts_old = 0
        self.timer = 0
        # Initialise subscribers and publishers
        self.feedback_sub = rospy.Subscriber("/encoder_counts", LeftRightInt32, self.feedback_speed, queue_size=10)
        self.input_sub = rospy.Subscriber("/target_speed", LeftRightFloat32, self.setpoint_update, queue_size = 1)
        self.output_pub = rospy.Publisher("/set_motor_duty_cycle", LeftRightFloat32, queue_size = 1)

    # Implement the subscriber callbacks
    def feedback_speed(self, msg):
        # Extracts wheel speed from the encoder node
        left_counts = -msg.left
        right_counts = -msg.right
        self.d_theta_left = ((left_counts - self.left_counts_old)/self.controller.cpr) * (2 * math.pi/TIMESTEP) #angular velocity, rad/s 
        self.d_theta_right = ((right_counts - self.right_counts_old)/self.controller.cpr) * (2 * math.pi/TIMESTEP)
        #calculates required voltages for each wheel using pid controller
        voltage_msg = LeftRightFloat32()
        voltage_msg.left = -self.controller.compute(setpoint = self.setpoint.left, feedback = self.d_theta_left) #wheel voltages are negative for forward operation
        voltage_msg.right = -self.controller.compute(setpoint = self.setpoint.right, feedback = self.d_theta_right)
        self.output_pub.publish(voltage_msg)
        self.left_counts_old = left_counts
        self.right_counts_old = right_counts
        
        #periodically clear integral, it blows up
        self.timer += 1
        if self.timer % 3 == 0:
            self.controller.integral = 0

        # Display the data
        rospy.loginfo("[SUBSCRIBER PY  NODE] received message with data = " + "left: {}, right: {}".format(self.setpoint.left, self.setpoint.right))
        rospy.loginfo("published message with data = " + "left: {}, right: {}".format(self.d_theta_left, self.d_theta_right))
        rospy.logerr("pwm L: {}, R: {}".format(voltage_msg.left, voltage_msg.right))
        rospy.logerr("motor error: {}, int error: {}, d error: {}, speed: {}".format(self.controller.error,self.controller.integral,self.controller.derivative,self.d_theta_left))

    #receives wheel velocity setpoint from trajectory controller
    def setpoint_update(self, msg):
        self.setpoint.left = msg.left
        self.setpoint.right = msg.right
        rospy.loginfo("setpoint = " + "left: {}, right: {}".format(self.setpoint.left, self.setpoint.right))
        

if __name__ == '__main__':

    global node_name
    node_name = "motor_controller"
    # Initialise the node
    rospy.init_node(node_name)
    # Display the namespace of the node handle
    rospy.loginfo("[SUBSCRIBER PY  NODE] namespace of node = " + rospy.get_namespace())
    # Start an instance of the class
    motor_controller = MotorControllerNode()
    # Spin as a single-threaded node
    rospy.spin()