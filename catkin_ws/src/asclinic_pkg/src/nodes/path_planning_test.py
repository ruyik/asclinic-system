#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy as np
from asclinic_pkg.msg import path_points
import math
from asclinic_pkg.msg import TemplateMessage


class path_planning_node:

    def __init__(self):
        self.publish_path = rospy.Publisher("/trajectory_description", path_points, queue_size=10)
        rospy.Subscriber("/next_point", TemplateMessage, self.path_planning, queue_size = 10)
        
        self.reachedEnd = False
        self.index = 0
     

    def path_planning(self, msg):
        #rospy.logerr("begin")
        self.reachedEnd = msg.temp_bool
        rate = rospy.Rate(1)
        
        path = path_points()
        previous = path_points()
        # previous.x = [-1,-1]
        # pervoius.y = [-1,-1]
        pathx = [0,0]
        pathy = [0,0]
       
        path_array = [(19, -9), (18.5, -9.0), (18.0, -9.0), (17.5, -9.0), (17.0, -9.0), (16.5, -9.0),
                    (16.0, -9.0), (16.0, -8.5), (16.0, -8.0)] #, (16.0, -7.5), (16.0, -7.0), (15.5, -7.0), (15.0, -7.0), (14.5, -7.0),
                    #(14.0, -7.0), (14.0, -7.5), (14.0, -8.0), (14.0, -9.0), (13.5, -9.0), (13.0, -9.0)]

        rospy.logerr("index = {}".format(self.index))


        #passes a line segment defined by start and end point            
        if self.index in range (0, len(path_array)-1):
            pathx[0] = path_array[self.index][0]
            pathy[0] = path_array[self.index][1]
            pathx[1] = path_array[self.index+1][0]
            pathy[1] = path_array[self.index+1][1]
            path.x = pathx
            path.y = pathy
            self.publish_path.publish(path)
            
        rate.sleep()
        if self.reachedEnd and previous != path:
            self.index = self.index + 1
            previous = path


        if self.reachedEnd and self.index == len(path_array):
            rospy.signal_shutdown("Finished entire trajectory")

        self.reachedEnd = False

if __name__ == '__main__':
    
    global node_name
    node_name = "path_planning"
    rospy.init_node(node_name)
    
    path_planning_node = path_planning_node()
    # # SPIN AS A SINGLE-THREADED NODE
    rospy.spin() 