#!/usr/bin/env python

import rospy
#import tf
import numpy as np
import matplotlib.pyplot as plt
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from math import pi, sqrt, atan2

import math
from math import pi, sqrt, atan2, atan
from geometry_msgs.msg import Vector3
from std_msgs.msg import Bool
import time
from collections import deque

def euler_from_quaternion(x, y, z, w):

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    
    return roll_x, pitch_y, yaw_z # in radians


class RobotMove:
    def __init__(self):
        # rospy.init_node('HLC', anonymous=False)
        rospy.loginfo("Press CTRL + C to terminate")
        self.arrive_flag = 0
        self.rotate_flag = 0
        self.x = 0.0
        self.y = 0.0
        self.record_x = deque([])
        self.record_y = deque([])
        self.starty = 0
        self.startx = 0
        self.theta = 0.0  # initialization
        self.avg_V = 0.45
        self.avg_V_ang = 0.4
        # record the position
        self.pos_array = np.array([[],[],[]])
        self.angle_array = np.array([])
        self.times = np.array([])

        # ID 3 : -4.12 , 1.48
        # ID   : -4.12-1.72 , 1.48+1.5
        #      : -4.34 , 4.98
        self.v = 0
        self.w = 0

        self.goal_list = deque([[-0.2,1],[-2.33,1],[-3.83,2.5],[-5.83,2.25],[-7.3,1.25]])
        self.goal_list_c = deque([[-0.2,1],[-2.33,1],[-3.83,2.5],[-5.83,2.25],[-7.3,1.25]])

        self.goal_list = deque([[-0.5,1],[-3,1],[-4,2.5],[-6.25,2.25]])
        self.goal_list_c = deque([[-0.5,1],[-3,1],[-4,2.5],[-6.25,2.25]])
        #self.goal_list = deque([[-0.2,1],[-2.33,1]])
        #self.goal_list_c = deque([[-0.2,1],[-2.33,1]])


        self.current_list = deque([])
        self.goal_x = 0
        self.goal_y = 0
        self.track_cl_flag = 0
        self.goal_theta = 0
        self.first_goal_flag = 0

        self.odom_sub = rospy.Subscriber("/asc"+"/odom", Odometry, self.odom_callback)
        self.vel_pub = rospy.Publisher("/asc"+"cmd_vel", Twist, queue_size=1)

        self.vel = Twist()
        self.rate = rospy.Rate(20)
        self.counter = 0
        self.trajectory = list()
        self.atun = 0
        rospy.sleep(1)


        #self.rotation_control(-1,0)
        #self.rotation_control(-2,1)
        
        for k in range(len(self.goal_list)-1):

            start_p = self.goal_list[k]
            dist_p = self.goal_list[k+1]
            # rotate:
            self.rotation_control(dist_p[0],dist_p[1])
            
            temp1,ref_L_frame,theta_ = self.calculate_L_frame(start_p[0],dist_p[0],start_p[1],dist_p[1])
            self.line_controller2(theta_,ref_L_frame,start_p[0],start_p[1],
                                 dist_p[0],dist_p[1])

        
        



        
        # track a point list: 

            

        '''
        while len(self.goal_list) != 0:
            if len(self.current_list) == 0:
                self.current_list.append([self.x,self.y])
                self.current_list.append(self.goal_list[0])
                self.current_list.append(self.goal_list[1])
                self.goal_list.popleft()
                self.goal_list.popleft()
                print('111',self.current_list)
            else:
                self.current_list.append(self.goal_list[0])
                self.goal_list.popleft()
                self.current_list.popleft()
                print('next goals',self.current_list)
            len_all = len(self.goal_list)
            print('len_all',len_all)
            start_point_X,start_point_Y = self.x,self.y
            angle,O,radius = self.calculate_angle([self.current_list[0][0],self.current_list[0][1]]
                                                  ,[self.current_list[1][0],self.current_list[1][1]]
                                             ,[self.current_list[2][0],self.current_list[2][1]],0.2)
            D_p2cut =  radius/np.tan(angle)
            temp1,ref_L_frame,theta_ = self.calculate_L_frame(self.current_list[0][0],self.current_list[1][0],self.current_list[0][1],self.current_list[1][1])
            print('first line control',ref_L_frame,theta_)
            self.line_controller(theta_,ref_L_frame,self.current_list[0][0],self.current_list[0][1],
                                 self.current_list[1][0],self.current_list[1][1],D_p2cut)
            self.track_circle_controller(radius,O,angle)
            if len_all == 0:
                temp1,ref_L_frame,theta_ = self.calculate_L_frame(self.current_list[1][0],self.current_list[2][0],self.current_list[1][1],self.current_list[2][1])
                print('second line control',ref_L_frame,theta_)
                self.line_controller2(theta_,ref_L_frame,self.current_list[1][0],self.current_list[1][1],
                                      self.current_list[2][0],self.current_list[2][1])
            '''
        
        

        
        # line control

        '''
        temp1,ref_L_frame,theta_ = self.calculate_L_frame(self.current_list[1][0],self.current_list[2][0],self.current_list[1][1],self.current_list[2][1])
        print('second line control',ref_L_frame,theta_)
        self.line_controller2(theta_,ref_L_frame,self.current_list[1][0],self.current_list[1][1],
                                self.current_list[2][0],self.current_list[2][1])
        '''




    def line_controller2(self,theta_,ref_L_frame,startx,starty,x_,y_):
        self.arrive_flag = 0
        while self.arrive_flag ==0:
            L_frame,temp2,temp3 = self.calculate_L_frame(startx,x_,starty,y_)
            #print('L_frame current',L_frame)

            print('high :  x:',self.x,' y:',self.y,' phi:',self.theta)
            #print(' phi:',theta_)

            K = np.array([0.1,0.05]) * 0.05
            Lframe_x = np.array([[L_frame[0][0]],[self.v]])
            temp_ = float(ref_L_frame[0])
            ref_Lx = np.array([[temp_],[0]])
            error_x = (ref_Lx - Lframe_x)
            change_x = np.dot(K,error_x)

            K = np.array([4,2])
            d_theta_L = self.theta - theta_

            
            if d_theta_L < -pi:
                d_theta_L = d_theta_L + 2*pi
            elif d_theta_L > pi:
                d_theta_L = d_theta_L - 2*pi
            
            #print('d_theta_L',d_theta_L)

            Lframe_y = np.array([[L_frame[1][0]],[d_theta_L]])

            ref_Ly = np.array([[0],[0]])
            error_y = (ref_Ly - Lframe_y)
            change_y = np.dot(K,error_y)
            new_v = change_x + self.v
            if new_v > self.avg_V-self.atun:
                new_v = self.avg_V-self.atun

            self.vel.linear.x =  new_v
            self.vel.angular.z = change_y

            if (ref_L_frame[0][0]-0.4 < L_frame[0][0]):
                self.atun = self.avg_V -0.05- self.avg_V * (ref_L_frame[0][0] - L_frame[0][0])
            else :
                self.atun = 0
            
            if (ref_L_frame[0][0]-0.07 < L_frame[0][0]):
                self.vel.angular.y = 0
                self.arrive_flag = 1
                self.vel.linear.x = 0
                self.vel.angular.z = 0

            self.vel_pub.publish(self.vel)
            self.rate.sleep()
        return 0
    def track_circle_controller(self,r_ref,O,angle):
        avg_v = self.avg_V_ang
        O = np.array(O)
        S = 0
        S_max = (pi/2 -angle )*2* r_ref
        self.track_cl_flag = 1
        #print('S_max',S_max)
        while self.track_cl_flag == 1:

            S += self.v /20
            #print('S',S)
            R_ = np.sqrt((self.x - O[0])**2 + (self.y - O[1])**2)
            K = np.array([-4,2])
            vector_2_odom =np.array([self.x,self.y]) - O
            norm_of_vec = np.linalg.norm(vector_2_odom)
            vector_2_edge = vector_2_odom/norm_of_vec * r_ref
            theta_ref = np.arctan2(vector_2_edge[1], vector_2_edge[0]) + pi/2
            if theta_ref > pi:
                theta_ref += -2*pi
            
            d_theta_L = self.theta - theta_ref
            if d_theta_L < -pi:
                d_theta_L = d_theta_L + 2*pi
            elif d_theta_L > pi:
                d_theta_L = d_theta_L - 2*pi
            current_fram_odom = np.array([[R_],[d_theta_L]])
            ref_both = np.array([[r_ref],[0]])
            error_ = (ref_both - current_fram_odom)
            change_y = np.dot(K,error_)
            #print('theta_ref:',theta_ref,'self.theta',self.theta,'R',R_,'error_theta',d_theta_L)
            scale = 1
            self.vel.linear.x =  (avg_v) * scale
            self.vel.angular.z = (avg_v/r_ref + change_y) * scale
            self.vel_pub.publish(self.vel)
            self.rate.sleep()
            if S > S_max-0.05:
                self.track_cl_flag = 0

            
        return 0

    def calculate_angle(self,A,B,C,dist):
        # Define the three points
        # Calculate the vectors AB and BC
        BA = np.array(A) - np.array(B)
        BC = np.array(C) - np.array(B)
        # Calculate the dot product of AB and BC
        dot_product = np.dot(BA, BC)
        # Calculate the magnitudes of AB and BC
        norm_BA = np.linalg.norm(BA)
        norm_BC = np.linalg.norm(BC)
        # Calculate the cosine of the angle between BA and BC
        cos_theta = dot_product / (norm_BA * norm_BC)
        # Calculate the angle in radians
        theta = np.arccos(cos_theta)
        theta = theta /2
        
        BA_normalizd = BA/norm_BA
        BC_normalizd = BC/norm_BC
        
        BO_ = BA_normalizd + BC_normalizd
        norm_BO_ = np.linalg.norm(BO_)
        BO_normalizd = BO_/norm_BO_
        
        # calcul r 
        r = dist/(1/np.sin(theta) - 1)
        
        BO = r/np.sin(theta) * BO_normalizd
        O = B + BO
        return theta,O,r
    

    # x_,y_ is the next goal point
    def line_controller(self,theta_,ref_L_frame,startx,starty,x_,y_,D_p2cut):
        self.arrive_flag = 0
        while self.arrive_flag ==0:
            L_frame,temp2,temp3 = self.calculate_L_frame(startx,x_,starty,y_)
            
            
            K = np.array([0.1,0.05]) * 0.1
            Lframe_x = np.array([[L_frame[0][0]],[self.v]])
            temp_1 = float(ref_L_frame[0])
            ref_Lx = np.array([[temp_1],[0]])
            error_x = (ref_Lx - Lframe_x)
            change_x = np.dot(K,error_x)

            K = np.array([2,1])
            d_theta_L = self.theta - theta_
            if d_theta_L < -pi:
                d_theta_L = d_theta_L + 2*pi
            elif d_theta_L > pi:
                d_theta_L = d_theta_L - 2*pi
            Lframe_y = np.array([[L_frame[1][0]],[d_theta_L]])

            ref_Ly = np.array([[0],[0]])
            error_y = (ref_Ly - Lframe_y)
            change_y = np.dot(K,error_y)
            new_v = change_x + self.v
            if new_v > self.avg_V-self.atun:
                new_v = self.avg_V-self.atun

            self.vel.linear.x =  new_v
            self.vel.angular.z = change_y

            if (ref_L_frame[0][0]-D_p2cut < L_frame[0][0]):
                self.rotate_flag = 0
                self.arrive_flag = 1
                self.vel.linear.x = 0
                self.vel.angular.z = 0

            self.vel_pub.publish(self.vel)
            self.rate.sleep()
        return 0

    # x_,y_ is the next goal point
    def rotation_control(self,x_,y_):
        self.rotate_flag = 0
        while self.rotate_flag == 0:
            temp1,temp2,theta_ = self.calculate_L_frame(self.x,x_,self.y,y_)
            errorthera = sqrt ((theta_ - self.theta)**2)

            K = np.array([0.1,0])
            state_theta = np.array([[self.theta],[self.w]])
            ref_state = np.array([[theta_],[0]])
            d_theta = theta_ - self.theta
            if d_theta < -pi:
                d_theta = d_theta + 2*pi
            elif d_theta > pi:
                d_theta = d_theta - 2*pi
            error_s = np.array([[d_theta],[0-self.w]])
            d_input = d_theta * 4
            self.vel.linear.x = 0
            self.vel.angular.y = 1
            self.vel.angular.z =  d_input
            print('high :  x:',self.x,' y:',self.y,' phi:',self.theta)

            if (d_theta**2 < 0.002):
                self.rotate_flag = 1
                self.vel.angular.y = 0
                self.vel.angular.z = 0
                temp1,ref_L_frame,theta_ = self.calculate_L_frame(self.startx,x_,self.starty,y_)
            self.vel_pub.publish(self.vel)
            self.rate.sleep()

        self.rotate_flag = 0
        # flag to tell low control
        self.vel.angular.y = 0
        return theta_,ref_L_frame

    def rotation_control2(self,x_,y_):
        self.rotate_flag = 0
        while self.rotate_flag == 0:
            temp1,temp2,theta_ = self.calculate_L_frame(self.x,x_,self.y,y_)
            errorthera = sqrt ((theta_ - self.theta)**2)
            K = np.array([1.25,1])
            state_theta = np.array([[self.theta],[self.w]])
            ref_state = np.array([[theta_],[0]])
            error_s = (ref_state - state_theta)
            d_input = np.dot(K,error_s)

            self.vel.linear.x = 0
            self.vel.angular.y = 1
            self.vel.angular.z =  d_input + self.w
            if (errorthera < 0.05):
                self.rotate_flag = 1
                self.vel.angular.z = 0
                self.vel.angular.y = 0
                temp1,ref_L_frame,theta_ = self.calculate_L_frame(self.startx,x_,self.starty,y_)
            self.vel_pub.publish(self.vel)
            self.rate.sleep()
        self.rotate_flag = 0
        # flag to tell low control
        self.vel.angular.y = 0
        return theta_,ref_L_frame


    def calculate_L_frame(self,x1,x2,y1,y2):
        r = 0
        if x2 > x1 and y2 > y1 :
            r = -atan((y2-y1)/(x2-x1))
        elif x2 < x1 and y2 < y1 : 
            r = -atan((y2-y1)/(x2-x1)) + pi
        elif x2 > x1 and y2 < y1 : 
            r = -atan((y2-y1)/(x2-x1))
        elif x2 < x1 and y2 > y1 : 
            r = -atan((y2-y1)/(x2-x1)) - pi
        elif x1 == x2 and y2 > y1:
            r = -pi/2
        elif x1 == x2 and y2 < y1:
            r = pi/2
        elif y1 == y2 and x2 > x1:
            r = 0
        elif y1 == y2 and x2 < x1:
            r = pi
        R = np.array([[np.cos(r) , -np.sin(r)],[np.sin(r) , np.cos(r)]])
        start_ = np.array([[x1],[y1]])
        odom_world_frame = np.array([[self.x],[self.y]])
        dist_in_world_frame = np.array([[x2],[y2]])
        L_frame = np.dot (R , (odom_world_frame - start_) )
        dist_in_L_frame =np.dot (R , (dist_in_world_frame - start_) )
        return L_frame ,dist_in_L_frame, -r
    
    
    def odom_callback(self, msg):
        # Get (x, y, phi) specification from odometry topic
        #print(msg)
        self.theta = msg.pose.pose.orientation.x
        self.x = msg.pose.pose.position.x
        self.y = msg.pose.pose.position.y
        #print('high level control callback',self.theta)
        '''
        if len(self.angle_array) < 6*50:
            print('11111')
            self.times = np.append(self.times,time.time())
            self.pos_array = np.append(self.pos_array,np.array([[self.x],[self.y],[0.2]]))
            self.angle_array = np.append(self.angle_array,self.theta)

        elif len(self.angle_array) == 6*50:
            np.savetxt('/home/asc09/asclinic-system-09/catkin_ws/odom_pos_array.out',self.pos_array,delimiter=',')
            np.savetxt('/home/asc09/asclinic-system-09/catkin_ws/odom_angle_array.out',self.angle_array,delimiter=',')
            np.savetxt('/home/asc09/asclinic-system-09/catkin_ws/odom_time.out',self.times ,delimiter=',')
        '''

        self.v = msg.twist.twist.linear.x
        self.w = msg.twist.twist.angular.z
        if self.first_goal_flag == 0:
            temp1,temp2,self.goal_phi = self.calculate_L_frame(self.x,self.goal_x,self.y,self.goal_y)

            self.first_goal_flag = 1
        
        # Make messages saved and prompted in 5Hz rather than 100Hz
        self.counter += 1
        if self.counter == 10:
            self.counter = 0
            self.trajectory.append([self.x,self.y])

            #rospy.loginfo("odom: x=" + str(self.x) + ";  y=" + str(self.y) + ";  phi=" + str(self.phi))
            #rospy.loginfo("goal: x=" + str(self.goal_x) + ";  y=" + str(self.goal_y) + ";  phi=" + str(self.goal_phi))


if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "robot_move"
    rospy.init_node(node_name)
    aruco_detector_object = RobotMove()
    # Spin as a single-threaded node
    rospy.spin()

