#!/usr/bin/env python

import rospy
#import tf
import numpy as np
import matplotlib.pyplot as plt
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from math import pi, sqrt, atan2

import math
from math import pi, sqrt, atan2, atan
from geometry_msgs.msg import Vector3
from std_msgs.msg import Bool
bool_msg = Bool()
bool_msg.data = False
def euler_from_quaternion(x, y, z, w):

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)
    
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)
    
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)
    
    return roll_x, pitch_y, yaw_z # in radians


class RobotMove:
    def __init__(self):
        # rospy.init_node('HLC', anonymous=False)
        self.arrive_flag = 0
        self.rotate_flag = 0
        self.x = 0.0
        self.y = 0.0
        self.starty = 0
        self.startx = 0
        self.phi = 0.0  # initialization
        self.v = 0
        self.w = 0

        self.goal_x = 3
        self.goal_y = 0

        self.goal_phi = 0
        self.first_goal_flag = 0

        self.odom_sub = rospy.Subscriber("/asc"+"/odom", Odometry, self.odom_callback)
    
        self.vel_pub = rospy.Publisher("/asc"+"cmd_vel", Twist, queue_size=1)
        
        self.arrive_flag_pub = rospy.Publisher('check_if_arrived', Bool, queue_size=1)
        self.vel = Twist()
        self.rate = rospy.Rate(20)
        self.counter = 0
        self.trajectory = list()
        self.atun = 0
        rospy.sleep(2)  
        while True: 

            errorthera = sqrt ((self.goal_phi - self.phi)**2)
            bool_msg.data = False
            '''
                if (self.rotate_flag == 0 and self.arrive_flag == 0):

                K = np.array([1.25,1])
                state_phi = np.array([[self.phi],[self.w]])
                ref_state = np.array([[self.goal_phi],[0]])
                error_s = (ref_state - state_phi)
                d_input = np.dot(K,error_s)

                self.vel.linear.x = 0
                self.vel.angular.z =  d_input + self.w
                if (errorthera < 0.05):
                    self.rotate_flag = 1
                    self.vel.angular.z = 0
                    self.startx = self.x
                    self.starty = self.y
                    temp1,ref_L_frame,self.goal_phi = self.calculate_L_frame(self.startx,self.goal_x,self.starty,self.goal_y)
                self.vel_pub.publish(self.vel)
                
            '''
            temp1,ref_L_frame,self.goal_phi = self.calculate_L_frame(self.startx,self.goal_x,self.starty,self.goal_y)
                
            if ( self.arrive_flag == 0):

                L_frame,temp2,temp3 = self.calculate_L_frame(self.startx,self.goal_x,self.starty,self.goal_y)
                K = np.array([0.1,0.05]) * 0.1
                Lframe_x = np.array([[L_frame[0][0]],[self.v]])
                abc=float(ref_L_frame[0])
                ref_Lx = np.array([[abc],[0]])
                error_x = (ref_Lx - Lframe_x)
                change_x = np.dot(K,error_x)

                K = np.array([1,0.5])
                d_phi_L = self.phi - self.goal_phi
                if d_phi_L < -pi:
                    d_phi_L = d_phi_L + 2*pi
                elif d_phi_L > pi:
                    d_phi_L = d_phi_L - 2*pi
                Lframe_y = np.array([[L_frame[1][0]],[d_phi_L]])

                ref_Ly = np.array([[0],[0]])
                error_y = (ref_Ly - Lframe_y)
                change_y = np.dot(K,error_y)
                new_v = change_x + self.v
                if new_v > 0.4-self.atun:
                    new_v = 0.4-self.atun

                self.vel.linear.x =  new_v
                self.vel.angular.z = change_y

                if (ref_L_frame[0][0]-1 < L_frame[0][0]):
                    self.atun = 0.3 - 0.4 * (ref_L_frame[0][0] - L_frame[0][0])
                else :
                    self.atun = 0
                
                if (ref_L_frame[0][0]-0.1 < L_frame[0][0]):
                    self.rotate_flag = 0
                    self.arrive_flag = 1
                    self.vel.linear.x = 0
                    self.vel.angular.z = 0
    
                self.counter += 1
                self.vel_pub.publish(self.vel)
                if self.counter > 10:
                    self.counter = 0
                    #print('L_frame',L_frame[0][0],L_frame[1][0])
                    #print('ref_L_frame',ref_L_frame[0][0],ref_L_frame[1][0])
            
            if ( self.arrive_flag == 1):
                self.vel.linear.x = 0
                self.vel.angular.z = 0
                self.vel_pub.publish(self.vel)
                bool_msg.data = True
                #print('ARRIVED _ ARRIVED _ARRIVED _ARRIVED _ARRIVED _')
            
            self.rate.sleep()


    def calculate_L_frame(self,x1,x2,y1,y2):
        r = 0
        if x2 > x1 and y2 > y1 :
            r = -atan((y2-y1)/(x2-x1))
        elif x2 < x1 and y2 < y1 : 
            r = -atan((y2-y1)/(x2-x1)) + pi
        elif x2 > x1 and y2 < y1 : 
            r = -atan((y2-y1)/(x2-x1))
        elif x2 < x1 and y2 > y1 : 
            r = -atan((y2-y1)/(x2-x1)) - pi
        R = np.array([[np.cos(r) , -np.sin(r)],[np.sin(r) , np.cos(r)]])
        start_ = np.array([[x1],[y1]])
        odom_world_frame = np.array([[self.x],[self.y]])
        dist_in_world_frame = np.array([[x2],[y2]])
        L_frame = np.dot (R , (odom_world_frame - start_) )
        dist_in_L_frame =np.dot (R , (dist_in_world_frame - start_) )
        return L_frame ,dist_in_L_frame, -r


    def odom_callback(self, msg):
        # Get (x, y, phi) specification from odometry topic

        self.phi = msg.pose.pose.orientation.x
        self.x = msg.pose.pose.position.x
        self.y = msg.pose.pose.position.y
        self.v = msg.twist.twist.linear.x
        self.w = msg.twist.twist.angular.z
        if self.first_goal_flag == 0:
            temp1,temp2,self.goal_phi = self.calculate_L_frame(self.x,self.goal_x,self.y,self.goal_y)

            self.first_goal_flag = 1
        print('high :  x:',self.x,'\n y:',self.y,'\n phi:',self.phi)
        # Make messages saved and prompted in 5Hz rather than 100Hz
        self.counter += 1
        if self.counter == 10:
            self.counter = 0
            self.trajectory.append([self.x,self.y])

            #rospy.loginfo("odom: x=" + str(self.x) + ";  y=" + str(self.y) + ";  phi=" + str(self.phi))
            #rospy.loginfo("goal: x=" + str(self.goal_x) + ";  y=" + str(self.goal_y) + ";  phi=" + str(self.goal_phi))


if __name__ == '__main__':
    # Initialise the node
    global node_name
    node_name = "robot_move"
    rospy.init_node(node_name)
    aruco_detector_object = RobotMove()
    # Spin as a single-threaded node
    rospy.spin()

