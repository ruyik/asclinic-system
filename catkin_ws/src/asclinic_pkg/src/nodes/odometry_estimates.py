#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import math
import numpy as np
from asclinic_pkg.msg import LeftRightInt32
from asclinic_pkg.msg import Pose
 
class Odometry:

  def __init__(self):
      self.radius = 0.072
      self.base = 0.109
      self.cpr = 4480
      self.x = 0
      self.z = 2.54
      self.phi = -np.pi
      self.kl = 0.0004
      self.kr = 0.0003
      self.cvk1 = 0.000002
      self.cvk2 = 0.000002
      self.Corvariance = 0
      self.Jacob_Pose = 0
      self.Jacob_Theta = 0
      self.var_odometry = 0
      # Initialise a subscriber
      sub_encoder = rospy.Subscriber("/asc/encoder_counts", LeftRightInt32, self.odometryCallback, queue_size=1)
      sub_cv = rospy.Subscriber("/asc/cv_pose", Pose, self.cvCallback, queue_size=1)
      # Initialise a publisher
      self.pose_publisher = rospy.Publisher("/asc/odom", Pose, queue_size=10)

  # Implement the subscriber callback
  def odometryCallback(self, msg):
      # Extract the data from the message
      left_counts = msg.left
      right_counts = msg.right
      d_theta_left = left_counts / self.cpr
      d_theta_right = right_counts / self.cpr
      ds = 0.5 * self.radius * (d_theta_left + d_theta_right)
      d_phi = (self.radius / (2 * self.base)) * (d_theta_right - d_theta_left)
      phi_ = self.phi
      self.phi = phi_ + d_phi
      self.x = self.x + ds * (math.cos(phi_ + 0.5 * self.phi))
      self.z = self.z + ds * (math.sin(phi_ + 0.5 * self.phi))

      self.Jacob_Pose = np.matrix([[1, 0, -ds * (math.sin(phi_ + 0.5 * self.phi))],
                                    [0, 1, ds * (math.cos(phi_ + 0.5 * self.phi))],
                                    [0, 0, 1]])
      A1 = math.cos(phi_ + 0.5 * self.phi)
      A2 = math.sin(phi_ + 0.5 * self.phi)
      B1 = self.radius / 2
      B2 = self.radius / (4 * self.base)
      self.Jacob_Theta = np.matrix([[B1 * A1 + B2 * ds * A2, B1 * A1 - B2 * ds * A2], 
                                    [B1 * A2 - B2 * ds * A1, B1 * A2 + B2 * ds * A1],
                                    [B2 * 2, -B2 * 2]])
      self.var_odometry = np.matrix([[self.kl * abs(d_theta_left), 0],
                                     [0, self.kr * abs(d_theta_right)]])
      # Display the data
      rospy.loginfo("[SUBSCRIBER PY  NODE] received message with data = " + "left: {}, right: {}".format(left_counts, right_counts))
      rospy.loginfo("Pose estimation = x: {}, z: {}, phi: {}".format(self.x, self.z, np.rad2deg(self.phi)))

  def cvCallback(self, msg):
      cv_x = msg.x
      cv_z = msg.z
      cv_phi = msg.phi

      self.x = float(self.x)
      self.z = float(self.z)
      self.phi = float(self.phi)

      if np.sign(np.rad2deg(cv_phi)) != np.sign(np.rad2deg(self.phi)) and ((np.rad2deg(cv_phi) > -180 and np.rad2deg(cv_phi) < -160) or (np.rad2deg(cv_phi) < 180 and np.rad2deg(cv_phi) > 160)):
        cv_phi = np.copysign(cv_phi, self.phi)

      cv_Pose = np.matrix([[cv_x], [cv_z], [cv_phi]])
      Var_z = np.matrix([[self.cvk1 * abs(cv_z), 0, 0],
                         [0, self.cvk1 * abs(cv_x), 0],
                         [0, 0, self.cvk2 * abs(cv_phi)]])
      Loc = np.matrix([[self.x], [self.z], [self.phi]])

      self.Corvariance = self.Jacob_Pose * self.Corvariance * np.transpose(self.Jacob_Pose) + self.Jacob_Theta * self.var_odometry * np.transpose(self.Jacob_Theta)
      K = self.Corvariance * np.linalg.inv(self.Corvariance + Var_z)
      Loc = Loc + K * (cv_Pose - Loc)
      self.Corvairnace = self.Corvariance - K * (self.Corvariance + Var_z) * np.transpose(K)
      
      self.x = Loc[0]
      self.z = Loc[1]
      self.phi = Loc[2]
      rospy.loginfo("Pose updates = x: {}, z: {}, phi: {}".format(self.x, self.z, np.rad2deg(self.phi)))

      pose = Pose()
      pose.x = self.x
      pose.z = self.z
      pose.phi = self.phi
      self.pose_publisher.publish(pose)

if __name__ == '__main__':
    global node_name
    node_name = "odometry_estimates"
    # Initialise the node
    rospy.init_node(node_name)
    # Display the namespace of the node handle
    rospy.loginfo("[SUBSCRIBER PY  NODE] namespace of node = " + rospy.get_namespace())
    # Start an instance of the class
    publisher_py_node = Odometry()
    # Spin as a single-threaded node
    rospy.spin()