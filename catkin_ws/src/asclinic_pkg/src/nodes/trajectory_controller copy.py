#!/usr/bin/env python
# -*- coding: utf-8 -*-
#trajectory controller takes input from trajectory_description (path planning), feedback from odometry, outputs to motor controller
#old

import rospy
import numpy as np
import time

from asclinic_pkg.msg import LeftRightFloat32


ANGULAR_SPEED = 1
TIMESTEP = 1/30
DURATION = 5
    
class motor_test:
    
    def __init__(self):
        # Initialise subscribers and publishers
        self.output_pub = rospy.Publisher("/target_speed", LeftRightFloat32, queue_size = 1)
        rospy.Timer(rospy.Duration(0.03), self.timerCallbackForPublishing)
        self.count = 0

    # Implement the subscriber callbacks
    def timerCallbackForPublishing(self, event):
        if self.count < DURATION/TIMESTEP:
            target_speed_msg = LeftRightFloat32()
            target_speed_msg.left = -ANGULAR_SPEED
            target_speed_msg.right = -ANGULAR_SPEED
        else:
            target_speed_msg.left = 0.0
            target_speed_msg.right = 0.0

        self.output_pub.publish(target_speed_msg)
        self.count = self.count + 1
        
        
        

   

if __name__ == '__main__':

    global node_name
    node_name = "motor_test"
    # Initialise the node
    rospy.init_node(node_name)
    # Display the namespace of the node handle
    rospy.loginfo("[SUBSCRIBER PY  NODE] namespace of node = " + rospy.get_namespace())
    # Start an instance of the class
    motor_test = motor_test()
    # Spin as a single-threaded node
    rospy.spin()

