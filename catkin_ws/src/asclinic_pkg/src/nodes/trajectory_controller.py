#!/usr/bin/env python
# -*- coding: utf-8 -*-
#trajectory controller takes input from trajectory_description (path planning), feedback from odometry, outputs to motor controller
#old

import rospy
import numpy as np
import time
from enum import Enum
from asclinic_pkg.msg import LeftRightInt32
from asclinic_pkg.msg import LeftRightFloat32
from asclinic_pkg.msg import TemplateMessage
from asclinic_pkg.msg import odometryPose
from asclinic_pkg.msg import path_points

STRAIGHT_SPEED = 0.5
ANGLE_THRESHOLD = 3 #angle difference in degrees required to exit turn state
TIMESTEP = 1/30

class States(Enum):
    STRAIGHT = 1
    TURN = 2
    STOP = 3
    PHOTO = 4
      

#to be replaced with a standard library for PID controllers.
class PIDController:
    def __init__(self, kp, ki, kd, min_output, max_output):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.min_output = min_output
        self.max_output = max_output
        self.setpoint = 0.0
        self.error = 0.0
        self.previous_error = 0.0
        self.integral = 0.0
        self.derivative = 0.0


    def compute(self, input, feedback): 
        self.error = input - feedback
        self.integral += self.error
        self.derivative = self.error - self.previous_error
        output = (self.kp * self.error) + (self.ki * self.integral) + (self.kd * self.derivative)
        output = min(max(output, self.min_output), self.max_output)
        self.previous_error = self.error
        return output
    

class TrajectoryControllerNode:
    
    def __init__(self):
        #80 percent seems to saturate 
        self.PPcontroller = PIDController(kp = 2, ki = 0.0, kd = 0.0, min_output = -0.5, max_output = 0.5) 
        self.HAcontroller = PIDController(kp = 1, ki = 0.0, kd = 0.0, min_output = -0.5, max_output = 0.5)
        self.state = States.STOP
        self.pose = odometryPose()
        #robot characteristics
        self.cpr = 2240
        self.radius = 0.072
        self.base = 0.109
        #pose information
        self.line_x = 0.0
        self.line_y = 0.0
        self.v = 0.0
        #time variables
        self.time = 0
        #path line segment information. holds line x and y dimensions, and angle of line. start and end hold coordinates of start and endpoints
        self.start, self.end = odometryPose(), odometryPose()
        #hard code first line segment and start heading
        self.pose.phi_p = -np.pi
        self.pose.x_p = 19.0
        self.pose.y_p = -9.0
        self.start.x_p = 19.0
        self.start.y_p = -9.0
        self.end.x_p = 18.5
        self.end.y_p = -9.0
        self.end.phi_p = np.pi
        #flag to get next line segment
        self.get_next = False
        #flag to take photo
        self.photo = False
        self.plant_ang = 0.0
        self.count = 0


        # Initialise subscribers and publishers
        self.feedback_sub = rospy.Subscriber("/odometry/pose_topic", odometryPose, self.main_callback, queue_size=1) #will require different msg syntax
        self.input_sub = rospy.Subscriber("/trajectory_description", path_points, self.path_update, queue_size = 1) #needs pose.msg that can store arrays
        self.camera_sub = rospy.Subscriber("/take_photo", TemplateMessage, self.camera_update, queue_size = 1)
        self.output_pub = rospy.Publisher("/target_speed", LeftRightFloat32, queue_size = 1)
        self.get_next_pub = rospy.Publisher("/next_point", TemplateMessage, queue_size = 10)


    #STATE TRANSITION LOGIC
    def get_state(self, photo):
        #state transition logic. camera is a variable passed by camera node to determine if a photo needs to be taken.
        #robot within a certain distance of target point.
        #otherwise a perpendicular infinite threshold at end of line segment; circles may never be reached
        if self.line_x >= self.length(a = self.start, b = self.end):
            self.state = States.STOP    
        elif self.get_next: 
            self.state = States.TURN
            if abs(self.pose.phi_p - self.end.phi_p) < np.deg2rad(ANGLE_THRESHOLD):
                #reset flag and timer. 
                self.get_next = False 
                self.time = 0     
        elif photo:
            self.state = States.PHOTO
        else:
            self.state = States.STRAIGHT

        return self.state


    def length(self, a, b):
        x = b.x_p - a.x_p
        y = b.y_p - b.y_p
        length = np.sqrt(x**2 + y**2)
        return length

    def angle(self, a, b):
        x = b.x_p - a.x_p
        y = b.y_p - a.y_p
        angle = np.arctan2(y,x)
        return angle

    def normalize(self,angle):
        if angle > np.pi:
            angle = angle - 2*np.pi
        elif angle < -np.pi:
            angle = angle + 2*np.pi
        return angle

    def param_path(self, speed):
        length = self.length(a = self.start, b = self.end)
        t = self.time
        x_ref = 0.0
        x_ref = min(speed*t*TIMESTEP,length)
        return x_ref


    # Implement the subscriber callbacks
    def main_callback(self, msg):
        # Extracts pose from odometry estimate. odometry is updated by kalman filter fusion of cv and encoder
        # odometry must publish frequently enough, otherwise this callback should be timer triggered: def main_callback(self, event):
        self.pose = msg
        target_speed_msg = LeftRightFloat32()
        get_next = TemplateMessage()
        path_ref = 0.0
        angle_ref = 0.0
        #increment time counter
        self.time += 1
        #check state, updates self.state
        self.get_state(photo = self.photo)
        
        #x and y coordinates in line frame
        length = self.length(a = self.start, b = self.pose)
        robot_angle = self.normalize(angle = self.angle(a = self.start, b = self.pose))
        #difference between robot angle from start and line segment angle
        theta = self.normalize(angle = robot_angle - self.end.phi_p)
        self.line_x = length * np.cos(theta)
        self.line_y = length * np.sin(theta)
        
        #State logic
        if(self.state == States.STRAIGHT):
            path_ref = self.param_path(speed = STRAIGHT_SPEED)
            angle_ref = 0.0
            v = self.PPcontroller.compute(input = path_ref, feedback = self.line_x)
            omega = self.HAcontroller.compute(input = angle_ref, feedback = self.line_y)
            #converting to angular wheel velocities
            target_speed_msg.left = v/self.radius - (self.base/2)*(omega/self.radius)
            target_speed_msg.right = v/self.radius + (self.base/2)*(omega/self.radius)

        elif(self.state == States.TURN):
            angle_ref = self.end.phi_p
            v = 0.0
            omega = self.HAcontroller.compute(input = angle_ref, feedback = self.pose.phi_p)
            target_speed_msg.left = v/self.radius - (self.base/2)*(omega/self.radius)
            target_speed_msg.right = v/self.radius + (self.base/2)*(omega/self.radius)

        elif(self.state == States.STOP):
            target_speed_msg.left = 0.0
            target_speed_msg.right = 0.0
            #flag path planning for next line segment
            self.get_next = True
            get_next.temp_bool = self.get_next
            self.get_next_pub.publish(get_next)

        elif(self.state == States.PHOTO):
            angle_ref = self.plant_ang
            v = 0.0
            omega = self.HAcontroller.compute(input = angle_ref, feedback = self.pose.phi_p)
            target_speed_msg.left = v/self.radius - (self.base/2)*(omega/self.radius)
            target_speed_msg.right = v/self.radius + (self.base/2)*(omega/self.radius)
            
        self.output_pub.publish(target_speed_msg)
        self.count = self.count + 1
        # if self.count%30 == 0:
        rospy.logerr("state is: {}, linex: {}, liney: {}, param: {}".format(self.state,self.line_x,self.line_y,path_ref))
        rospy.logerr("desired wheel speed L: = {}, R = {}".format(target_speed_msg.left,target_speed_msg.right))
        rospy.logerr("current pose: x = {}, y = {}".format(msg.x_p,msg.y_p))
        #rospy.logerr("length: {}, angle: {}, pose angle: {}, theta: {}, end: {}".format(length,robot_angle,self.pose.phi_p, theta, self.end.phi_p))
        rospy.logerr("start point: x = {}, y = {}, end point: x = {}, y = {}".format(self.start.x_p, self.start.y_p, self.end.x_p, self.end.y_p))
        

    #updates path information whenever path planning publishes
    def path_update(self, msg):
        self.start.x_p = msg.x[0]
        self.start.y_p = msg.y[0] 
        self.end.x_p = msg.x[1]
        self.end.y_p = msg.y[1]
        #calculate angle of line segment from start point in world frame
        self.end.phi_p = self.normalize(angle = self.angle(a = self.start, b = self.end))

    
    def camera_update(self, msg):
        self.photo = msg.temp_bool
        self.plant_ang = msg.temp_float 
        

if __name__ == '__main__':

    global node_name
    node_name = "trajectory_controller"
    # Initialise the node
    rospy.init_node(node_name)
    # Display the namespace of the node handle
    rospy.loginfo("[SUBSCRIBER PY  NODE] namespace of node = " + rospy.get_namespace())
    # Start an instance of the class
    trajectory_controller_object = TrajectoryControllerNode()
    # Spin as a single-threaded node
    rospy.spin()

