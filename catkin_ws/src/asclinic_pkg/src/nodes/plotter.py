#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import matplotlib.pyplot as plt

from asclinic_pkg.msg import odometryPose

# Global variables to store the trajectory
x_traj = []
y_traj = []

def odom_callback(odom_msg):
    global x_traj, y_traj

    # Extract the robot's position from the odometry message
    x = odom_msg.x_p
    y = odom_msg.y_p

    # Append the position to the trajectory
    x_traj.append(x)
    y_traj.append(y)

def plot_trajectory():
    global x_traj, y_traj

    # Initialize the ROS node
    rospy.init_node('trajectory_plotter', anonymous=True)

    # Subscribe to the odometry topic
    rospy.Subscriber('/odometry/pose_topic', odometryPose, odom_callback)

    # Wait for the first odometry message to arrive
    rospy.wait_for_message('/odometry/pose_topic', odometryPose)

    # Plot the trajectory
    plt.plot(x_traj, y_traj)
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Robot Trajectory')
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
    try:
        plot_trajectory()
    except rospy.ROSInterruptException:
        pass
